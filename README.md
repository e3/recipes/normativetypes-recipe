# normativeTypes conda recipe

Home: https://github.com/epics-base/normativeTypesCPP

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: normativeTypes EPICS V4 C++ module
